#include "usermode.h"
#include "ui_usermode.h"

#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QDir>
#include <QProcess>
#include <QDebug>
#include <QDateTime>
#include <QDesktopServices>
#include <QStringList>
#include <QStringListModel>
#include <QEventLoop>
#include <QTimer>
#include <QThread>
#include <QFile>
#include <QMessageBox>
#include <QKeyEvent>

#include "common.h"
#include "treemodel.h"

#include "qtcsv/stringdata.h"
#include "qtcsv/reader.h"
#include "qtcsv/writer.h"

#include "logger.h"

CUserMode::CUserMode(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserMode)
{
    ui->setupUi(this);
    ui->m_swUserModePages->setCurrentIndex(0);
    CLogger::getInstance()->log("Switched to User Mode",QtDebugMsg);

}

CUserMode::~CUserMode()
{
    delete ui;
}

void CUserMode::on_m_pbDroneAttackFrameWork_clicked()
{

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");

    m_lstAllDrones =  setting.childGroups();
    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);
    ui->m_cmbxDroneList->setModel(m_lstDronesListModel);

    setting.endGroup();


    ui->m_swUserModePages->setCurrentIndex(ui->m_swUserModePages->currentIndex()+1);

    CLogger::getInstance()->log("Switched to Drone Attack frame work",QtDebugMsg);

}

void CUserMode::on_m_pbBack_clicked()
{
    emit sigUserModeBack();
    CLogger::getInstance()->log("Switched to DRAT main UI",QtDebugMsg);

}

void CUserMode::on_m_pbNext_clicked()
{
    on_m_pbDroneAttackFrameWork_clicked();
    CLogger::getInstance()->log("Switched to Drone Attack framw work",QtDebugMsg);

}

void CUserMode::on_m_pbBackAttack_clicked()
{
    ui->m_swUserModePages->setCurrentWidget(ui->m_pageAvailableDrones);
    CLogger::getInstance()->log("Switched to available drones page.",QtDebugMsg);

}

void CUserMode::on_m_pbBackAvailableDrones_clicked()
{
    ui->m_swUserModePages->setCurrentIndex(ui->m_swUserModePages->currentIndex()-1);
    CLogger::getInstance()->log("Switched to Drone Attack framw work",QtDebugMsg);

}

void CUserMode::on_m_pbNextAvailableDrones_clicked()
{

    ui->m_leDroneSSID->clear();
    ui->m_leMacID->clear();


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");

    m_lstAllDrones =  setting.childGroups();
    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);

    setting.endGroup();

    ui->m_cmbxSelectedDrone->setModel(m_lstDronesListModel);
    ui->m_cmbxSelectedDrone->setCurrentIndex(ui->m_cmbxDroneList->currentIndex() );


    QString strSelectedDrone = ui->m_cmbxDroneList->currentText();
    setting.beginGroup("DronesList");

    setting.beginGroup(strSelectedDrone);

    QString strSelectedDroneMacAddress=setting.value("DroneMacAddress","00:00:00:00").toString();
    setting.endGroup();
    setting.endGroup();

    ui->m_leMacID->setText(strSelectedDroneMacAddress);

    ui->m_swUserModePages->setCurrentIndex(ui->m_swUserModePages->currentIndex()+1);
}

void CUserMode::on_m_pbScanDrone_clicked()
{

    if(ui->m_pbScanDrone->text() == "Attack Instructions")
    {

        if(ui->m_cmbxSelectInterface->currentText() == "USB(GPS)" )
        {
            QDesktopServices::openUrl(QUrl(g_GPSInstructionFilePath));
        }else if(ui->m_cmbxSelectInterface->currentText() == "USB(Bluetooth)")
        {
            QDesktopServices::openUrl(QUrl(g_BluetoothInstructionFilePath));
        }
        return;
    }
    //iwlist wlan scan > wlan.txt

    QProcess OProcess;
    QString Command;    //Contains the command to be executed
    QStringList args;   //Contains arguments of the command

    QString strFilename=g_WLANscanListFilePath;
    Command = "iwlist";
    args<<"wlan0" <<"scan";


    OProcess.start(Command,args,QIODevice::ReadWrite); //Starts execution of command
    OProcess.waitForFinished();                       //Waits for execution to complete

    QString StdOut      =   OProcess.readAllStandardOutput();  //Reads standard output
    QString StdError    =   OProcess.readAllStandardError();   //Reads standard error

    qDebug()<<"\n Printing the standard output..........\n";
    qDebug()<<endl<<StdOut;


    QFile file(strFilename);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        // We're going to streaming text to the file
        QTextStream stream(&file);
        stream << QDateTime::currentDateTime().toString("YYYYMMMdd hh::mm:ss.zzz");
        stream << "UserMode";
        stream << StdOut;
        stream << StdError;
        stream << endl;


        file.close();
        qDebug() << "Writing finished";
    }



    qDebug()<<"\n Printing the standard error..........\n";
    qDebug()<<endl<<StdError;

//    QMessageBox::information(this,"Alert","Scanned data saved successfully to "+strFilename);
    QMessageBox::information(this,"Alert","Scanned data saved successfully.");

    QDesktopServices::openUrl(QUrl(g_WLANscanListFilePath));

    CLogger::getInstance()->log("Scan drone operation completed.",QtDebugMsg);


}

void CUserMode::on_m_pbConnectDrone_clicked()
{

    QString strSSID=ui->m_leDroneSSID->text();


    QString strMacID=ui->m_leMacID->text();


    QString strAttack=ui->m_cmbAttack->currentText();

    if(strSSID.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please select drone SSID");
        return;
    }

    if(strMacID.isEmpty() )
    {
        QMessageBox::warning(this, "Alert", "Please select drone MAC id");
        return;
    }

    if(!strMacID.contains(":") )
    {
        QMessageBox::warning(this, "Alert", "Please enter correct MAC id");
        return;
    }
    QString strSelectedDrone=ui->m_cmbxSelectedDrone->currentText();
    if(strSelectedDrone.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please add Drone");
        return;
    }


    //gnome-terminal -- bash -c "/root/Documents/Dronetest/Scripts/tcp_msg.py; exec bash"

    QString strScriptPath= QDir::homePath()+QDir::separator()+"Documents/Dronetest/Scripts/Tello_connect.py";

    QProcess *process= new QProcess;
    QString strpython="gnome-terminal";
    QStringList lstArg;
    lstArg << "--" << "bash"  << "-c" << " "+strScriptPath+"; exec bash" ;

    qDebug() << strpython << strScriptPath;
    process->startDetached(strpython,lstArg);


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");
    setting.beginGroup(strSelectedDrone);

    setting.setValue("MACAddesss",strMacID);
    setting.setValue("SSID",strSSID);

    setting.endGroup();
    setting.endGroup();

    setting.sync();


    QEventLoop loop;
    QTimer::singleShot(4*1000,&loop,SLOT(quit()));
    loop.exec();


    QMessageBox::information(this,"Alert","connected successfully.");
    CLogger::getInstance()->log("conneted drone operation completed.",QtDebugMsg);

}

void CUserMode::on_m_pbBackConnectivity_clicked()
{
    ui->m_swUserModePages->setCurrentIndex(ui->m_swUserModePages->currentIndex()-1);
}

void CUserMode::on_m_pbNextConnectivity_clicked()
{
    ui->m_swUserModePages->setCurrentIndex(ui->m_swUserModePages->currentIndex()+1);
    ui->m_radAttackNotSuccessful->setChecked(true);
}

void CUserMode::on_m_cmbAttackList_currentTextChanged(const QString &arg1)
{
    m_mapAttackNameScriptPath .clear();
    QString strSelectedDrone=ui->m_cmbxSelectedDrone->currentText();

   // qDebug() << "Selected Drone name " << strSelectedDrone;


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");



    QStringList attackNames;
    QStringList  lstAttack =  setting.childGroups();

    setting.endGroup();


  //  qDebug() << "attack list " << lstAttack;

    for (auto attackname:lstAttack)
    {
        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);

       // qDebug() << "attack name " << attackname;

        QString strDroneName=setting.value("DroneName","").toString();

       // qDebug() << "Drone name " << strDroneName;

        if(strDroneName != strSelectedDrone)
        {
            setting.endGroup();
            setting.endGroup();


            continue;
        }

        QString strAttackType=setting.value("AttackType","").toString();
      //  qDebug() << "Attack Type " << strAttackType;


        if( strAttackType != arg1 )
        {
            setting.endGroup();
            setting.endGroup();

            continue;
        }


        QString strAttackName= setting.value("AttackName","").toString();
        QString strAttackScriptPath= setting.value("AttackScriptPath","").toString();

      //  qDebug() << "Eligible Attack Name " << strAttackName << "  --------script path:" << strAttackScriptPath;



        m_mapAttackNameScriptPath.insert(strAttackName,strAttackScriptPath);
        attackNames << strAttackName;

        setting.endGroup();
        setting.endGroup();

    }

   // qDebug() << "Drone Specific" << attackNames;

     ui->m_cmbAttack->clear();

    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(attackNames);
    ui->m_cmbAttack->setModel(m_lstDronesListModel);

}

void CUserMode::on_m_pbSubmitAttack_clicked()
{

    QString strAttack=ui->m_cmbAttack->currentText();

    if(strAttack.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please select attack");
        return;
    }

    //gnome-terminal -- bash -c "/root/Documents/Dronetest/Scripts/tcp_msg.py; exec bash"

    QString strScriptPath=m_mapAttackNameScriptPath.value(strAttack);

    QProcess *process= new QProcess;
    QString strpython="gnome-terminal";
    QStringList lstArg;
    lstArg << "--" << "bash"  << "-c" << " "+strScriptPath+"; exec bash" ;

    qDebug() << strpython << strScriptPath;
    process->startDetached(strpython,lstArg);

    updateAttackStatus();

    CLogger::getInstance()->log("Attack submitted",QtDebugMsg);
}

void CUserMode::on_m_cmbxSelectInterface_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "USB(GPS)" || arg1 == "USB(Bluetooth)")
    {
        ui->m_pbConnectDrone->setEnabled(false);
        ui->m_pbScanDrone->setText("Attack Instructions");
        ui->m_leDroneSSID->setEnabled(false);
        ui->m_lblSelectedSSID->setEnabled(false);
        ui->m_lblMACid->setEnabled(false);
        ui->m_leMacID->setEnabled(false);
       // ui->m_pbNextConnectivity->setEnabled(true);
    }else
    {
        if(ui->m_leMacID->text().isEmpty() || ui->m_leDroneSSID->text().isEmpty())
        {
            ui->m_pbConnectDrone->setEnabled(false);
        }else
        {
            ui->m_pbConnectDrone->setEnabled(true);
        }

        ui->m_pbScanDrone->setText("Scan");
        ui->m_leDroneSSID->setEnabled(true);
        ui->m_lblSelectedSSID->setEnabled(true);
        ui->m_lblMACid->setEnabled(true);
        ui->m_leMacID->setEnabled(true);
      //  ui->m_pbNextConnectivity->setEnabled(false);
    }
}

void CUserMode::on_m_leDroneSSID_textChanged(const QString &arg1)
{
    if(ui->m_leMacID->text().isEmpty() || ui->m_leDroneSSID->text().isEmpty())
    {
        ui->m_pbConnectDrone->setEnabled(false);
    }else
    {
        ui->m_pbConnectDrone->setEnabled(true);
    }
}

//void CUserMode::on_m_leMacID_textChanged(const QString &arg1)
//{
//    if(ui->m_leMacID->text().isEmpty() || ui->m_leDroneSSID->text().isEmpty())
//    {
//        ui->m_pbConnectDrone->setEnabled(false);
//    }else
//    {
//        ui->m_pbConnectDrone->setEnabled(true);
//    }
//}

void CUserMode::on_m_cmbxDroneList_currentTextChanged(const QString &arg1)
{
    QString strSelectedDrone=arg1;

   // qDebug() << "Selected Drone name " << strSelectedDrone;


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");



    QStringList attackNames;
    QStringList  lstAttack =  setting.childGroups();

    setting.endGroup();


  //  qDebug() << "attack list " << lstAttack;

    for (auto attackname:lstAttack)
    {
        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);

       // qDebug() << "attack name " << attackname;

        QString strDroneName=setting.value("DroneName","").toString();

       // qDebug() << "Drone name " << strDroneName;

        if(strDroneName != strSelectedDrone)
        {
            setting.endGroup();
            setting.endGroup();


            continue;
        }

//        QString strAttackType=setting.value("AttackType","").toString();
//      //  qDebug() << "Attack Type " << strAttackType;


//        if( strAttackType != arg1 )
//        {
//            setting.endGroup();
//            setting.endGroup();

//            continue;
//        }


        QString strAttackName= setting.value("AttackName","").toString();
        QString strAttackScriptPath= setting.value("AttackScriptPath","").toString();

      //  qDebug() << "Eligible Attack Name " << strAttackName << "  --------script path:" << strAttackScriptPath;



        attackNames << strAttackName;

        setting.endGroup();
        setting.endGroup();

    }

   // qDebug() << "Drone Specific" << attackNames;

    ui->m_lblNoOfAvailableAttacks->setText(QString::number(attackNames.count()));

}

void CUserMode::on_m_btnReports_clicked()
{

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");
    QStringList lstAllDrones =  setting.childGroups();
    setting.endGroup();

    if(lstAllDrones.count()<1)
    {
        QMessageBox::warning(this, "Alert", "Please add drones");
        return;
    }


      ui->m_swUserModePages->setCurrentWidget(ui->m_pageReports);
      loadReports();

      CLogger::getInstance()->log("Switched to Reports UI",QtDebugMsg);

}

void CUserMode::on_m_btnSearch_clicked()
{

  QString searchKey=ui->m_leSearchItem->text();
  loadReports(searchKey);

}

void CUserMode::loadReports(QString strFilterKey)
{
    QStringList m_lstDeckItemHeaders ;

    m_lstDeckItemHeaders << "DroneName(MAC ID)" << "";
    TreeModel *theMomdel = new TreeModel(m_lstDeckItemHeaders,this);

    QMap<QString,QMap<QString,QString>> m_ProjectDetails;

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);

    setting.beginGroup("AttackList");

    QStringList attackNames;
    QStringList  lstAttack =  setting.childGroups();

    setting.endGroup();


    for (auto attackname:lstAttack)
    {
        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);


        QString strDroneName=setting.value("DroneName","").toString();

//        qDebug() << "Attack Name:" << attackname << "Drone Name:" << strDroneName;


        setting.endGroup();
        setting.endGroup();


        if(strDroneName.isEmpty())
        {
            continue;
        }

        setting.beginGroup("DronesList");

        setting.beginGroup(strDroneName);


        QString strDroneMacAddress=setting.value("DroneMacAddress","").toString();

        strDroneName+="("+strDroneMacAddress+")";

        setting.endGroup();
        setting.endGroup();


        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);


       QMap<QString,QString> attackDetails;


       QString strAttackType=setting.value("AttackType","").toString();
       QString strAttackName=setting.value("AttackName","").toString();
       QString strAttackScriptPath=setting.value("AttackScriptPath","").toString();

        if(strFilterKey.isEmpty() || strDroneName.contains(strFilterKey) )
        {
            if(m_ProjectDetails.contains(strDroneName))
            {

                QMap<QString,QString> lstAttackMap= m_ProjectDetails.value(strDroneName);
                int nAttackCount= lstAttackMap.size()/3;
                QString strAttackCount= QString::number(nAttackCount+1);

                lstAttackMap.insert("Attack"+strAttackCount+"Type",strAttackType );
                lstAttackMap.insert("Attack"+strAttackCount+"Name",strAttackName );
                lstAttackMap.insert("Attack"+strAttackCount+"ScriptPath",strAttackScriptPath );

                m_ProjectDetails[strDroneName] = lstAttackMap;


            }else

            {
                attackDetails.insert("Attack1Type",strAttackType );
                attackDetails.insert("Attack1Name",strAttackName );
                attackDetails.insert("Attack1ScriptPath",strAttackScriptPath );
                m_ProjectDetails.insert(strDroneName,attackDetails );
            }
        }


        setting.endGroup();

        setting.endGroup();

    }


    theMomdel->setupModelData(m_ProjectDetails);

    ui->m_treeViewData->setModel(theMomdel);

    for (int column = 0; column < theMomdel->columnCount(); ++column)
        ui->m_treeViewData->resizeColumnToContents(column);

    ui->m_treeViewData->expandToDepth(1);
    ui->m_treeViewData->expandAll();


}

void CUserMode::on_m_pbReportsBack_clicked()
{
    ui->m_swUserModePages->setCurrentWidget(ui->m_pageUserMode);
}

void CUserMode::on_m_btnExportToCSV_clicked()
{

    if(QFile::exists(g_csvReportPath))
    {
        QFile::remove(g_csvReportPath);
    }

    QDir dir;
    if(!dir.exists(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Reports/"))
    {
        dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Reports/");
    }


    // prepare data that you want to save to csv-file
    QStringList strList;
    strList << "DroneName" << "MACAddress" << "AttackName" << "AttackType" << "AttackStatus";

    QtCSV::StringData strData;
//    strData.addEmptyRow();
    strData << strList ;

    QStringList m_lstDeckItemHeaders ;


    QMap<QString,QMap<QString,QString>> m_ProjectDetails;

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);

    setting.beginGroup("AttackList");

    QStringList attackNames;
    QStringList  lstAttack =  setting.childGroups();

    setting.endGroup();


    for (auto attackname:lstAttack)
    {
        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);

        QString strDroneName=setting.value("DroneName","").toString();

        QString strAttackType=setting.value("AttackType","").toString();
        QString strAttackNamestrDroneName=setting.value("AttackName","").toString();
        QString strAttackStatus=setting.value("AttackStatus","").toString();

        setting.endGroup();
        setting.endGroup();



        setting.beginGroup("DronesList");
        setting.beginGroup(strDroneName);


        QString strDroneMacAddress=setting.value("DroneMacAddress","").toString();


        setting.endGroup();
        setting.endGroup();


        QStringList strListData;
        strListData << strDroneName << strDroneMacAddress << strAttackNamestrDroneName << strAttackType << strAttackStatus;
        strData << strListData ;


    }


    // write to file
    QString filePath = g_csvReportPath;

    if(QFile::exists(g_csvReportPath))
    {
        QFile::remove(g_csvReportPath);
    }

    QtCSV::Writer::write(filePath, strData);


    QMessageBox::warning(this, "Alert", "Report saved successfully.");


    CLogger::getInstance()->log("Exported data to CSV file",QtDebugMsg);

}


void CUserMode::on_m_radAttackNotSuccessful_clicked()
{
    updateAttackStatus();
}

void CUserMode::on_m_radAttackSuccessful_clicked()
{
    updateAttackStatus();
}

void CUserMode::updateAttackStatus(bool bState)
{

    QString strAttack=ui->m_cmbAttack->currentText();
    if(strAttack.isEmpty())
    {
        return;
    }

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");
    setting.beginGroup(strAttack);
    if(ui->m_radAttackSuccessful->isChecked())
    {
        setting.setValue("AttackStatus","Successful");
    }else
    {
        setting.setValue("AttackStatus","Not Successful");
    }

    setting.endGroup();
    setting.endGroup();

    if(bState)
    {
        QMessageBox::information(this, "Alert", "Attack status updated.");
    }

    CLogger::getInstance()->log("Update attack status",QtDebugMsg);

}

bool CUserMode::eventFilter(QObject* obj, QEvent* event)
{
    if (event->type()==QEvent::KeyPress)
    {
        QKeyEvent* key = static_cast<QKeyEvent*>(event);
        if ( (key->key()==Qt::Key_Enter) || (key->key()==Qt::Key_Return) )
        {
            //Enter or return was pressed

            if(ui->m_leSearchItem->hasFocus())
            {
               on_m_btnSearch_clicked();
            }

        } else
        {
            return QObject::eventFilter(obj, event);
        }
        return true;
    } else
    {
        return QObject::eventFilter(obj, event);
    }
    return false;
}
