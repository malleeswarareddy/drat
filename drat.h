#ifndef CDRAT_H
#define CDRAT_H

/**
**This file conatains all developer mode & user mode  functionality.
**/

#include "usermode.h"
#include "developermode.h"

#include <QMainWindow>
#include <QString>

QT_BEGIN_NAMESPACE
namespace Ui { class DRAT; }
QT_END_NAMESPACE

class CDRAT : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief CDRAT - constructor and initializer list
     * @param parent
     */
    CDRAT(QWidget *parent = nullptr);

    /**
      ~CDRAT() - destructor
      */

    ~CDRAT();

public slots:
    /**
     * @brief slotGoToWelcomePage : used for to launch User mode to drat UI or developer mode to drat UI.
     */
    void slotGoToWelcomePage();
private slots:

    /**
     * @brief on_m_cmbxSelectmode_currentIndexChanged - select mode based on user selection
     * @param index
     */
    void on_m_cmbxSelectmode_currentIndexChanged(int index);

    void on_m_pbReadmeFile_clicked();

    void on_m_pbAboutPageBack_clicked();

private:
    Ui::DRAT *ui;
    CUserMode *theUserMode=nullptr;
    CDeveloperMode *theDevMode=nullptr;
};
#endif // CDRAT_H
