#ifndef USERMODE_H
#define USERMODE_H

/**
**This file conatains all user mode  functionality.
**/

#include <QDialog>
#include <QStringListModel>

namespace Ui {
class UserMode;
}

class CUserMode : public QDialog
{
    Q_OBJECT

public:

    /**
     * @brief CUserMode : constructor of CUserMode class
     * @param parent
     */
    explicit CUserMode(QWidget *parent = nullptr);
    /**
     * @brief ~CUserMode : destructor of CUserMode class
     */
    ~CUserMode();

private slots:

    /**
     * @brief on_m_pbDroneAttackFrameWork_clicked : Switched to Drone Attack frame work
     */
    void on_m_pbDroneAttackFrameWork_clicked();

    /**
     * @brief on_m_pbBack_clicked : This function is used to navigate user mode to DRAT UI
     */
    void on_m_pbBack_clicked();

    /**
     * @brief on_m_pbNext_clicked : This function used to navigate to Drone Attack frame work UI
     */
    void on_m_pbNext_clicked();

    /**
     * @brief on_m_pbBackAttack_clicked : This function is used to navigate Available Drones page
     */
    void on_m_pbBackAttack_clicked();

    /**
     * @brief on_m_pbBackAvailableDrones_clicked : This function is used to naviagate user Mode page
     */
    void on_m_pbBackAvailableDrones_clicked();


    /**
     * @brief on_m_pbNextAvailableDrones_clicked :  This function is used to navigate Connectivity page
     */
    void on_m_pbNextAvailableDrones_clicked();

    /**
     * @brief on_m_pbScanDrone_clicked : This function is used to store all wlan information on the system to wlanscanlist.txt file uisng
     *                                   "iwlist wlan0 scan" command
     */
    void on_m_pbScanDrone_clicked();

    /**
     * @brief on_m_pbConnectDrone_clicked : This function is used to connect drone using Tello_connect.py script file on bash terminal
     */
    void on_m_pbConnectDrone_clicked();

    /**
     * @brief on_m_pbBackConnectivity_clicked : This function is used to naviagate connectivity page to Available Drones page
     */
    void on_m_pbBackConnectivity_clicked();

    /**
     * @brief on_m_pbNextConnectivity_clicked : This function is used to navigate Attack List page
     */
    void on_m_pbNextConnectivity_clicked();

    /**
     * @brief on_m_cmbAttackList_currentTextChanged : This function is used update drone list page
     * @param arg1
     */
    void on_m_cmbAttackList_currentTextChanged(const QString &arg1);

    /**
     * @brief on_m_pbSubmitAttack_clicked : This function is used to execute attack functionality w.r.t script file
     */
    void on_m_pbSubmitAttack_clicked();

    /**
     * @brief on_m_cmbxSelectInterface_currentIndexChanged : This function is used to enable/ disbale controls on UI
     * @param arg1
     */
    void on_m_cmbxSelectInterface_currentIndexChanged(const QString &arg1);

    /**
     * @brief on_m_leDroneSSID_textChanged :  This function is used to enable/ disbale controls on UI
     * @param arg1
     */
    void on_m_leDroneSSID_textChanged(const QString &arg1);

    /**
     * @brief on_m_cmbxDroneList_currentTextChanged :  This function is used show attack list count page  w.r.t drone selection
     * @param arg1
     */
    void on_m_cmbxDroneList_currentTextChanged(const QString &arg1);


    /**
     * @brief on_m_btnReports_clicked : This function is used to navigate Reports page UI
     */
    void on_m_btnReports_clicked();


    /**
     * @brief on_m_btnSearch_clicked : This function is used to filter drones on tree view
     */
    void on_m_btnSearch_clicked();

    /**
     * @brief loadReports : This function is used to load report page content on treeview
     * @param strFilterKey
     */
    void loadReports(QString strFilterKey="");

    /**
     * @brief on_m_pbReportsBack_clicked : This function is used to navigate UI user mode page
     */
    void on_m_pbReportsBack_clicked();

    /**
     * @brief on_m_btnExportToCSV_clicked : This function is used to export treeview page conetent to CSV file
     */
    void on_m_btnExportToCSV_clicked();

    /**
     * @brief on_m_radAttackNotSuccessful_clicked :  This function is used to store Attack Not successful information to data base(dratsettings.ini) file.
     */
    void on_m_radAttackNotSuccessful_clicked();

    /**
     * @brief on_m_radAttackSuccessful_clicked : This function is used to store Attack successful information to data base(dratsettings.ini) file.
     */
    void on_m_radAttackSuccessful_clicked();


signals:

    /**
     * @brief sigUserModeBack : This signal is used to navigate UserMode page to DRAT UI page.
     */
    void sigUserModeBack();

protected:
    bool eventFilter(QObject* obj, QEvent* event) override;

private:

    Ui::UserMode *ui;
    QStringListModel *m_lstDronesListModel;
    QStringList m_lstAllDrones;
    QMap<QString,QString> m_mapAttackNameScriptPath;

    void updateAttackStatus(bool bState=false);
};

#endif // USERMODE_H
