#include "logger.h"
#include <QTextStream>
#include <QDateTime>
#include <QDir>


Q_LOGGING_CATEGORY(logDebug,    "Debug")
Q_LOGGING_CATEGORY(logInfo,     "Info")
Q_LOGGING_CATEGORY(logWarning,  "Warning")
Q_LOGGING_CATEGORY(logCritical, "Critical")
Q_LOGGING_CATEGORY(logFatal, "Fatal")


QScopedPointer<QFile> m_logFile;

CLogger* CLogger::m_logger = nullptr;

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Open stream file writes
    QTextStream out(m_logFile.data());
    // Write the date of recording
    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
    // By type determine to what level belongs message
    switch (type)
    {
    case QtInfoMsg:     out << "INF "; break;
    case QtDebugMsg:    out << "DBG "; break;
    case QtWarningMsg:  out << "WRN "; break;
    case QtCriticalMsg: out << "CRT "; break;
    case QtFatalMsg:    out << "FTL "; break;
    }
    // Write to the output category of the message and the message itself
    out << context.category << ": "
        << msg << endl;
    out.flush();    // Clear the buffered data
}

CLogger::CLogger(QObject *parent) : QObject(parent)
{
    QDir dir;
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Log");
    m_strLogFileName = QDir::homePath()+QDir::separator()+"Documents/Dronetest/Log/"+"log.txt";

    if(QFile::exists(m_strLogFileName))
    {
        QFile file(m_strLogFileName);
        file.rename(m_strLogFileName+"_"+QDateTime::currentDateTime().toString("yyyyMMMdd_hh:mm:ss.zzz"));
    }

    // Set the logging file
       // check which a path to file you use
       m_logFile.reset(new QFile(m_strLogFileName));
       // Open the file logging
       m_logFile.data()->open(QFile::WriteOnly | QFile::Text);
       // Set handler
       qInstallMessageHandler(messageHandler);

}

CLogger::~CLogger()
{
    if(m_logFile->isOpen())
    {
        m_logFile->close();
    }
}

CLogger *CLogger::getInstance()
{
    if(m_logger == nullptr)
    {
        m_logger = new CLogger;
    }
    return m_logger;
}

void CLogger::deleteInstance()
{
    if(m_logger == nullptr)
    {
        delete  m_logger;
    }

}

void CLogger::log(QString strData,QtMsgType type)
{

    switch (type)
    {
    case QtInfoMsg:
    {
        qInfo(logInfo()) << strData;

    }break;
    case QtDebugMsg:
    {
        qDebug(logDebug()) << strData;

    }break;
    case QtWarningMsg:
    {
        qCritical(logCritical()) <<strData;

    }break;
    case QtCriticalMsg:
    {
        qWarning(logWarning()) << strData;

    }break;
    case QtFatalMsg:
    {
        qWarning(logFatal()) << strData;

    }
        break;

    }


}
