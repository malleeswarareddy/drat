#Qt modules
QT       += core gui
#Qt modules
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#to use C++11 standard
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#required cpp files
SOURCES += \
    developermode.cpp \
    logger.cpp \
    main.cpp \
    drat.cpp \
    treeitem.cpp \
    treemodel.cpp \
    usermode.cpp

#required header(.h) files
HEADERS += \
    common.h \
    developermode.h \
    drat.h \
    logger.h \
    treeitem.h \
    treemodel.h \
    usermode.h

#required UI(.ui) files
FORMS += \
    developermode.ui \
    drat.ui \
    usermode.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HOME = $$system(echo $HOME)
#The compiled exe will generate at HOME/Documents/Dronetest/

unix:DESTDIR      =  $$HOME/Documents/Dronetest/
OBJECTS_DIR = $$PWD/Temp/obj
MOC_DIR =     $$PWD/Temp/moc
RCC_DIR =     $$PWD/Temp/rcc
UI_DIR =      $$PWD/Temp/gui

#CSV library used for to create CSV reports
include($$PWD/qtcsv/qtcsv.pri)

#resources files al dependedet html, tools , scripts
RESOURCES += \
    resources.qrc
