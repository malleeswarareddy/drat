#ifndef DEVELOPERMODE_H
#define DEVELOPERMODE_H

/**
**This file conatains all developer mode functionality.
**/
#include <QDialog>
#include <QStringListModel>

namespace Ui {
class DeveloperMode;
}

class CDeveloperMode : public QDialog
{
    Q_OBJECT

public:

    /**
     * @brief CDeveloperMode : constructor of CDeveloperMode class
     * @param parent
     */
    explicit CDeveloperMode(QWidget *parent = nullptr);


    /**
     * @brief ~CDeveloperMode : destructor of CDeveloperMode class
     */
    ~CDeveloperMode();

private slots:

    /**
     * @brief on_m_pbAddDrone_clicked : This function is used to switch to add drone page in stack widget
     */
    void on_m_pbAddDrone_clicked();

    /**
     * @brief on_m_pbRemoveDrone_clicked : This function is used to switch to remove drone page in stack widget
     */
    void on_m_pbRemoveDrone_clicked();

    /**
     * @brief on_m_pbNext_clicked : This function is used to switch to add drone page in stack widget
     */
    void on_m_pbNext_clicked();

    /**
     * @brief on_m_pbBack_clicked : This function is used to switch to dev mode options page in stack widget
     */
    void on_m_pbBack_clicked();

    /**
     * @brief on_m_pbAddDroneBack_clicked : This function is used to switch add drone page to  dev mode options page in stack widget
     */
    void on_m_pbAddDroneBack_clicked();

    /**
     * @brief on_m_pbDevModeNRemoveDroneBack_clicked :  This function is used to switch remove drone page to  dev mode options page in stack widget
     */
    void on_m_pbDevModeNRemoveDroneBack_clicked();

    /**
     * @brief on_m_pbDone_clicked :  This function is used to store new drone information to database(dratsettings.ini) file
     */
    void on_m_pbDone_clicked();

    /**
     * @brief on_m_pbDevModeNRemoveDroneBack_clicked :  This function is used to switch New Attackpage to  dev mode options page in stack widget
     */
    void on_m_pbDevModeNewAttackBack_clicked();

    /**
     * @brief on_m_pbDone_clicked :  This function is used to remove selected drone information from database(dratsettings.ini) permanently
     */
    void on_m_pbRemoveDoneOK_clicked();

    /**
     * @brief on_m_pbManageDrone_clicked : This function is used to navigate ui to manage drone page in stack widget
     */
    void on_m_pbManageDrone_clicked();


    /**
     * @brief on_m_pbAddAttacks_clicked : This function is used Add Attacks to database(dratsettings.ini) w.r.t to selected drone
     */
    void on_m_pbAddAttacks_clicked();

    /**
     * @brief on_m_pbDevModeOptionsBack_clicked : This function is used to switch Dev Mode to DRAT ui main page
     */

    void on_m_pbDevModeOptionsBack_clicked();

    /**
     * @brief on_m_pbScan_clicked :  This function is used to store all wlan information on the system to wlanscanlist.txt file uisng
     *                               "iwlist wlan0 scan" command
     */
    void on_m_pbScan_clicked();

    /**
     * @brief on_m_pbNewAttackScriptUpload_clicked : This function is used to select attack script file from UI
     */
    void on_m_pbNewAttackScriptUpload_clicked();

    /**
     * @brief on_m_pbAttackDone_clicked: This function is used to save attack information to database(dratsettings.ini) w.r.t to selected drone
     */
    void on_m_pbAttackDone_clicked();

    /**
     * @brief on_m_pbRemoveAttack_clicked :  This function is used to remove attack information from database(dratsettings.ini) w.r.t to selected drone
     */
    void on_m_pbRemoveAttack_clicked();

    /**
     * @brief on_m_pbRemoveAttack_clicked :  This function is used to remove attack all information from database(dratsettings.ini) w.r.t to all drones
     */
    void on_m_pbRemoveAllAttacks_clicked();

    /**
     * @brief on_m_pbManageAttacks_clicked :  This function  is used to naviagte ui to Manage Attacks page(Add/Remove)
     */
    void on_m_pbManageAttacks_clicked();

    /**
     * @brief on_m_pbBackManageAttacks_clicked : This function is used to naviagate UI from ManageAttacks to Dev Mode options page on stack widget
     */
    void on_m_pbBackManageAttacks_clicked();

    /**
     * @brief on_m_pbDevModeNRemoveAttackBack_clicked : This function is used to naviagate UI from Remove Attack page to Manage Attack page on stack widget
     */
    void on_m_pbDevModeNRemoveAttackBack_clicked();

    /**
     * @brief on_m_pbRemoveAttackApply_clicked :  This function is used to remove attack information from database(dratsettings.ini) w.r.t to selected drone
     */
    void on_m_pbRemoveAttackApply_clicked();

    /**
     * @brief on_m_chkRemoveAllAttacks_clicked :  This function is used to remove attack all information from database(dratsettings.ini) w.r.t to all drones
     * @param checked: true/ false
     */
    void on_m_chkRemoveAllAttacks_clicked(bool checked);

    /**
     * @brief on_m_pbRemoveAllDrones_clicked :  This function is used to remove all drones & attack information from database(dratsettings.ini)
     */
    void on_m_pbRemoveAllDrones_clicked();

    /**
     * @brief on_m_cmbRemoveAttackDrone_currentIndexChanged :This function is used to remove attack information from database(dratsettings.ini) w.r.t to selected drone
     * @param arg1 : selected attack name
     */
    void on_m_cmbRemoveAttackDrone_currentIndexChanged(const QString &arg1);

    /**
     * @brief on_m_pbAddTools_clicked : This function is used to add tools infromation to database(dratsettings.ini)
     */

    void on_m_pbAddTools_clicked();

    /**
     * @brief on_m_pbAddTools_clicked : This function is used to navigate manage tools page on statck widget
     */

    void on_m_pbManageTools_clicked();
    
    /**
     * @brief on_m_pbAddTools_clicked :  This function is used to navigate remove tools page on statck widget
     */
    void on_m_pbRemoveTools_clicked();

    /**
     * @brief on_m_pbAddTools_clicked : This function used to naviagte UI to launch tool page. This function is used to launch selected tool from UI
     */
    void on_m_pblaunchTools_clicked();

    /**
     * @brief on_m_pbAddNewTool_clicked : This function is used to add new tool information to database(dratsettings.ini)
     */
    void on_m_pbAddNewTool_clicked();

    /**
     * @brief on_m_pbAddToolBack_clicked :  This function is used navigate UI form AddTool to Manage Tool page
     */
    void on_m_pbAddToolBack_clicked();


    /**
     * @brief on_m_pbRemoveTool_clicked : This function is used to remove selected tool from  database(dratsettings.ini)
     */
    void on_m_pbRemoveTool_clicked();

    /**
     * @brief on_m_pbRemoveToolBack_clicked : This function is used navigate UI form Remove Tool page to Manage Tool page
     */
    void on_m_pbRemoveToolBack_clicked();

    /**
     * @brief on_m_pbLaunchTool_clicked:  This function is used to launch selected tool from UI
     */
    void on_m_pbLaunchTool_clicked();

    /**
     * @brief on_m_pbLaunchToolBack_clicked :  This function is used navigate UI form Launch Tool page to Manage Tool page
     */
    void on_m_pbLaunchToolBack_clicked();


    /**
     * @brief on_m_pbBackManageTools_clicked : This function is used navigate UI form Manage Tool page to Dev Mode options page
     */
    void on_m_pbBackManageTools_clicked();

    /**
     * @brief on_m_pbNextManageTools_clicked : This function is used navigate UI form Manage Tool page to Add Tool page
     */
    void on_m_pbNextManageTools_clicked();

    /**
     * @brief on_m_pbBrowse_clicked : This function is used to select tool from O.S
     */
    void on_m_pbBrowse_clicked();

    /**
     * @brief on_m_chkShowAddRemoveTools_clicked :This functionnis is used hide/ show controls on manage tool page
     * @param checked
     */
    void on_m_chkShowAddRemoveTools_clicked(bool checked);

    /**
     * @brief on_m_pbToolInfo_clicked :  This function is used to show Tools_Info.txt file
     */
    void on_m_pbToolInfo_clicked();

signals:

    /**
     * @brief sigDevModeBack : This function is used to navigate UI from Dev Mode to DRAT UI
     */
    void sigDevModeBack();

private:

    Ui::DeveloperMode *ui;
    QStringListModel *m_lstDronesListModel;
    QStringList m_lstAllDrones;
    QStringList m_lstAllTools;
    QStringListModel *m_lstToolsListModel;

};

#endif // DEVELOPERMODE_H
