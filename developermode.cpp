// File Description :
//
#include "developermode.h"
#include "ui_developermode.h"

#include "common.h"

#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QDir>
#include <QProcess>
#include <QDebug>
#include <QDateTime>
#include <QDesktopServices>
#include <QTimer>

#include "logger.h"

CDeveloperMode::CDeveloperMode(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeveloperMode)
{
    ui->setupUi(this);
    ui->m_swDevMode->setCurrentWidget(ui->DevModeOptions);
    ui->m_pbAddTools->setVisible(false);
    ui->m_pbRemoveTools->setVisible(false);
    ui->m_pbNextManageTools->setVisible(false);



    CLogger::getInstance()->log("Switched to Admin Mode",QtDebugMsg);

    //Add default tool to resources

    QString strName="dh-lin-x64-v1.11";
    QString strToolPath=g_hackTooPath;


    bool bToolNameAlreadyExists=false;
    QSettings settingCheck(g_appSettingsPath,QSettings::NativeFormat);
    settingCheck.beginGroup("ToolList");
    QStringList lstAllTools =  settingCheck.childGroups();

    if(lstAllTools.contains(strName))
    {
        bToolNameAlreadyExists =true;
    }

    settingCheck.endGroup();


    if(!bToolNameAlreadyExists)
    {

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("ToolList");
    setting.beginGroup(strName);
    setting.setValue("ToolName",strName);
    setting.setValue("ToolPath",strToolPath);
    setting.endGroup();
    setting.endGroup();

    setting.sync();

   }
}

CDeveloperMode::~CDeveloperMode()
{
    delete ui;
}

void CDeveloperMode::on_m_pbAddDrone_clicked()
{
    ui->m_leDroneName->clear();
    ui->m_leDroneImagePath->clear();
    ui->m_teDroneAddDescription->clear();

    ui->m_swDevMode->setCurrentWidget(ui->DevModeAddDrone);
    CLogger::getInstance()->log("Switched to Add Drone UI",QtDebugMsg);

}

void CDeveloperMode::on_m_pbRemoveDrone_clicked()
{

    QString strDrone=ui->m_cmbRemoveDrone->currentText();

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");

    m_lstAllDrones =  setting.childGroups();
    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);
    ui->m_cmbRemoveDrone->setModel(m_lstDronesListModel);

    setting.endGroup();


    ui->m_swDevMode->setCurrentWidget(ui->DevModeRemoveDrone);

    CLogger::getInstance()->log("Switched to Remove Drone UI",QtDebugMsg);


}

void CDeveloperMode::on_m_pbNext_clicked()
{
    on_m_pbAddDrone_clicked();
    CLogger::getInstance()->log("Switched to Add Drone UI",QtDebugMsg);
}

void CDeveloperMode::on_m_pbBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeOptions);

    CLogger::getInstance()->log("Switched to Admin Mode Options UI",QtDebugMsg);
}

void CDeveloperMode::on_m_pbAddDroneBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageDrone);
    CLogger::getInstance()->log("Switched to Manage Drone Mode UI",QtDebugMsg);

}

void CDeveloperMode::on_m_pbDevModeNRemoveDroneBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageDrone);
    CLogger::getInstance()->log("Switched to Manage Drone Mode UI",QtDebugMsg);

}

void CDeveloperMode::on_m_pbDone_clicked()
{

    QString strDroneName=ui->m_leDroneName->text();

    if(strDroneName.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please enter drone name");
        return;
    }

    QString strDroneDescription=ui->m_teDroneAddDescription->toPlainText();
    if(strDroneDescription.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please enter description");
        return;
    }


    QString strDroneImagePath=ui->m_leDroneImagePath->text();




    if(strDroneImagePath.isEmpty() )
    {
        QMessageBox::warning(this, "Alert", "Please enter drone MAC id");
        return;
    }

    if(!strDroneImagePath.contains(":") )
    {
        QMessageBox::warning(this, "Alert", "Please enter correct MAC id");
        return;
    }


    bool bDroneNameAlreadyExists=false;
    QSettings settingCheck(g_appSettingsPath,QSettings::NativeFormat);
    settingCheck.beginGroup("DronesList");
    QStringList lstAllDrones =  settingCheck.childGroups();

    if(lstAllDrones.contains(strDroneName))
    {
        bDroneNameAlreadyExists =true;
    }

    settingCheck.endGroup();


    if(bDroneNameAlreadyExists)
    {
        QMessageBox::warning(this, "Alert", "Drone name and SSID already exists");
        return;
    }


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");
    setting.beginGroup(strDroneName);
    setting.setValue("DroneName",strDroneName);
    setting.setValue("DroneDescription",strDroneDescription);
    setting.setValue("DroneMacAddress",strDroneImagePath);
    setting.endGroup();
    setting.endGroup();

    setting.sync();
    QMessageBox::warning(this, "Alert", "Successfully added.");


    QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Attack", "Do you want to add attack to this drone?",
                                    QMessageBox::Yes|QMessageBox::No);
      if (reply == QMessageBox::Yes)
      {
          ui->m_leNeAttackName->clear();
          //ui->m_leNewAttackSSID->clear();
          ui->m_leScriptPath->clear();
          ui->m_teNewAttackDescription->clear();

         QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
         setting.beginGroup("DronesList");

         m_lstAllDrones =  setting.childGroups();

         m_lstDronesListModel = new QStringListModel;
         m_lstDronesListModel->setStringList(m_lstAllDrones);
         ui->m_cmbDroneName->clear();
         ui->m_cmbDroneName->setModel(m_lstDronesListModel);

         setting.endGroup();



          ui->m_swDevMode->setCurrentWidget(ui->DevModeNewAttack);
      }else
      {
          ui->m_swDevMode->setCurrentWidget(ui->DevModeManageDrone);
      }

      CLogger::getInstance()->log("Drone Added.",QtDebugMsg);


}


void CDeveloperMode::on_m_pbDevModeNewAttackBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeOptions);

    CLogger::getInstance()->log("Switched to Admin Mode options.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbRemoveDoneOK_clicked()
{

    if(ui->m_chkRemoveAllDrones->isChecked())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Attack", "Do you want to remove all drones?",
                                      QMessageBox::Yes|QMessageBox::No);
        if (reply != QMessageBox::Yes)
        {
            return;
        }
    }

    QString strSelectedDrone= ui->m_cmbRemoveDrone->currentText();
    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");
    if(ui->m_chkRemoveAllDrones->isChecked())
    {
        setting.remove("");
    }else
    {

       setting.remove(strSelectedDrone);

    }
    setting.endGroup();

    //QMessageBox::information(this,"Alert","Drone data removd successfully from"+g_appSettingsPath);
    QMessageBox::information(this,"Alert","Drone data removd successfully.");

    setting.beginGroup("DronesList");


    setting.endGroup();


    if(ui->m_chkRemoveAllDrones->isChecked())
    {

        setting.beginGroup("AttackList");
        setting.remove("");
        setting.endGroup();

    }else
    {


    setting.beginGroup("AttackList");

    QStringList attackNames;
    QStringList  lstAttack =  setting.childGroups();

    setting.endGroup();


    for (auto attackname:lstAttack)
    {
        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);

       // qDebug() << "attack name " << attackname;

        QString strDroneName=setting.value("DroneName","").toString();

       // qDebug() << "Drone name " << strDroneName;

        if(strDroneName != strSelectedDrone)
        {
            setting.endGroup();
            setting.endGroup();


            continue;
        }

//        QString strAttackName= setting.value("AttackName","").toString();
//        setting.remove(strAttackName);


        setting.endGroup();

        setting.remove(attackname);

        setting.endGroup();

    }

    }

    setting.beginGroup("DronesList");

    m_lstAllDrones =  setting.childGroups();
    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);
    ui->m_cmbRemoveDrone->clear();
    ui->m_cmbRemoveDrone->setModel(m_lstDronesListModel);
    setting.endGroup();

    CLogger::getInstance()->log("Removal of drone completed. ",QtDebugMsg);



}

void CDeveloperMode::on_m_pbManageDrone_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageDrone);
    CLogger::getInstance()->log("Switched to Manage Drone UI",QtDebugMsg);

}

void CDeveloperMode::on_m_pbAddAttacks_clicked()
{

    ui->m_leNeAttackName->clear();
    //ui->m_leNewAttackSSID->clear();
    ui->m_leScriptPath->clear();
    ui->m_teNewAttackDescription->clear();

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");

    m_lstAllDrones =  setting.childGroups();

    //qDebug() << m_lstAllDrones;

    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);
    ui->m_cmbDroneName->clear();
    ui->m_cmbDroneName->setModel(m_lstDronesListModel);

    setting.endGroup();

    ui->m_swDevMode->setCurrentWidget(ui->DevModeNewAttack);

    CLogger::getInstance()->log("Switched to Add Attacks UI",QtDebugMsg);

}

void CDeveloperMode::on_m_pbDevModeOptionsBack_clicked()
{
    emit sigDevModeBack();
    CLogger::getInstance()->log("Switched to DRAT main UI",QtDebugMsg);
}


void CDeveloperMode::on_m_pbScan_clicked()
{
    QProcess OProcess;
    QString Command;    //Contains the command to be executed
    QStringList args;   //Contains arguments of the command

    QString strFilename=g_WLANscanListFilePath;
    Command = "iwlist";
    args<<"wlan0" <<"scan";


    OProcess.start(Command,args,QIODevice::ReadWrite); //Starts execution of command
    OProcess.waitForFinished();                       //Waits for execution to complete

    QString StdOut      =   OProcess.readAllStandardOutput();  //Reads standard output
    QString StdError    =   OProcess.readAllStandardError();   //Reads standard error

    qDebug()<<"\n Printing the standard output..........\n";
    qDebug()<<endl<<StdOut;


    QFile file(strFilename);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        // We're going to streaming text to the file
        QTextStream stream(&file);
        stream << QDateTime::currentDateTime().toString("YYYYMMMdd hh::mm:ss.zzz");
        stream << "UserMode";
        stream << StdOut;
        stream << StdError;
        stream << endl;


        file.close();
        qDebug() << "Writing finished";
    }



    qDebug()<<"\n Printing the standard error..........\n";
    qDebug()<<endl<<StdError;

   // QMessageBox::information(this,"Alert","Scanned data saved successfully to "+strFilename);
     QMessageBox::information(this,"Alert","Scanned data saved successfully.");

     QDesktopServices::openUrl(QUrl(g_WLANscanListFilePath));

     CLogger::getInstance()->log("Scan operation completed.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbNewAttackScriptUpload_clicked()
{

    QString strFileName = QFileDialog::getOpenFileName(this,
        tr("Open Script file"), QDir::homePath()+QDir::separator()+"Documents/Dronetest/Scripts", tr("Script Files (*.py *.sh)"));

    ui->m_leScriptPath->setText(strFileName);

    CLogger::getInstance()->log("Selected script file",QtDebugMsg);

}

void CDeveloperMode::on_m_pbAttackDone_clicked()
{


    QString strAttackName=ui->m_leNeAttackName->text();

    if(strAttackName.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please enter attack name");
        return;
    }

    QString strAttackDescription=ui->m_teNewAttackDescription->toPlainText();

    if(strAttackDescription.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please enter attack description");
        return;
    }

    QString strSelectedDrone=ui->m_cmbDroneName->currentText();

    if(strSelectedDrone.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please add drone");
        return;
    }


    QString strAttackScriptPath=ui->m_leScriptPath->text();

    if(strAttackScriptPath.isEmpty())
    {
        QMessageBox::warning(this, "Alert", "Please select attack script file");
        return;
    }


    bool bDroneNameAlreadyExists=false;
    QSettings settingCheck(g_appSettingsPath,QSettings::NativeFormat);
    settingCheck.beginGroup("AttackList");
    QStringList lstAllDrones =  settingCheck.childGroups();

    if(lstAllDrones.contains(strAttackName))
    {
        bDroneNameAlreadyExists =true;
    }

    settingCheck.endGroup();


    if(bDroneNameAlreadyExists)
    {
        QMessageBox::warning(this, "Alert", "Attack name and SSID already exists");
        return;
    }

    QString strAttackType=ui->m_cmbxAttackType->currentText();


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");
    setting.beginGroup(strAttackName);
    setting.setValue("AttackName",strAttackName);
    setting.setValue("DroneName",strSelectedDrone);
    setting.setValue("AttackType",strAttackType);
    setting.setValue("AttackScriptPath",strAttackScriptPath);
    setting.setValue("AttackDescription",strAttackScriptPath);
    setting.endGroup();
    setting.endGroup();
    setting.sync();


   // QMessageBox::warning(this, "Alert", "Attack Successfully added to "+g_appSettingsPath);
     QMessageBox::warning(this, "Alert", "Attack Successfully added.");

    ui->m_swDevMode->setCurrentWidget(ui->DevModeOptions);

    CLogger::getInstance()->log("Attack added succssfully.",QtDebugMsg);


}


void CDeveloperMode::on_m_pbRemoveAttack_clicked()
{

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");
    m_lstAllDrones =  setting.childGroups();

//    qDebug() << __FUNCTION__ << m_lstAllDrones.join("") << endl;
    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);
  //S  ui->m_cmbRemoveAttackDrone->clear();
    ui->m_cmbRemoveAttackDrone->setModel(m_lstDronesListModel);

    setting.endGroup();

    ui->m_swDevMode->setCurrentWidget(ui->DevModeRemoveAttacks);

    CLogger::getInstance()->log("Switched to UI attack page.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbRemoveAllAttacks_clicked()
{

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Attack", "Do you want to remove all attacks?",
                                    QMessageBox::Yes|QMessageBox::No);
      if (reply != QMessageBox::Yes)
      {
          return;
      }

//    ui->m_swDevMode->setCurrentWidget(ui->DevModeRemoveAttacks);
    QString strSelectedDrone=ui->m_cmbRemoveAttackDrone->currentText();

    QString strSelectedAttack=ui->m_cmbxAttackDrone->currentText();

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");

    if(ui->m_chkRemoveAllAttacks->isChecked())
    {
        setting.remove("");
    }else
    {
        setting.remove(strSelectedAttack);
    }

    setting.endGroup();

    CLogger::getInstance()->log("All attacks removed.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbManageAttacks_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageAttacks);
    CLogger::getInstance()->log("Switched to Manage Attack UI.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbBackManageAttacks_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeOptions);
    CLogger::getInstance()->log("Switched to Admon Mode options UI.",QtDebugMsg);
}

void CDeveloperMode::on_m_pbDevModeNRemoveAttackBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageAttacks);
    CLogger::getInstance()->log("Switched to Manage Attack UI.",QtDebugMsg);
}

void CDeveloperMode::on_m_pbRemoveAttackApply_clicked()
{

    if(ui->m_chkRemoveAllAttacks->isChecked())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Attack", "Do you want to remove all attacks?",
                                      QMessageBox::Yes|QMessageBox::No);
        if (reply != QMessageBox::Yes)
        {
            return;
        }
    }


    QString strSelectedDrone=ui->m_cmbRemoveAttackDrone->currentText();

    QString strSelectedAttack=ui->m_cmbxAttackDrone->currentText();

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");

    if(ui->m_chkRemoveAllAttacks->isChecked())
    {
        setting.remove("");
    }else
    {
        setting.remove(strSelectedAttack);
    }

    setting.endGroup();

    on_m_cmbRemoveAttackDrone_currentIndexChanged(strSelectedDrone);

    CLogger::getInstance()->log("Remove attack operation completed.",QtDebugMsg);

}

void CDeveloperMode::on_m_chkRemoveAllAttacks_clicked(bool checked)
{
    if(checked)
    {
        ui->m_lblAttackDrone->setEnabled(false);
        ui->m_cmbxAttackDrone->setEnabled(false);
    }else
    {
        ui->m_lblAttackDrone->setEnabled(true);
        ui->m_cmbxAttackDrone->setEnabled(true);
    }
}

void CDeveloperMode::on_m_pbRemoveAllDrones_clicked()
{

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Attack", "Do you want to remove all drones?",
                                    QMessageBox::Yes|QMessageBox::No);
   if (reply != QMessageBox::Yes)
   {
      return;
   }

    QString strSelectedDrone= ui->m_cmbRemoveDrone->currentText();

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("DronesList");
    setting.remove("");
    setting.endGroup();

    //QMessageBox::information(this,"Alert","Drone data removd successfully from"+g_appSettingsPath);
    QMessageBox::information(this,"Alert","Drone data removd successfully.");

    setting.beginGroup("DronesList");

    m_lstAllDrones =  setting.childGroups();
    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(m_lstAllDrones);
    ui->m_cmbRemoveDrone->clear();
    ui->m_cmbRemoveDrone->setModel(m_lstDronesListModel);

    setting.endGroup();

    setting.beginGroup("AttackList");
    setting.remove("");
    setting.endGroup();

    CLogger::getInstance()->log("Removal of Drone and attack operation completed.",QtDebugMsg);

}

void CDeveloperMode::on_m_cmbRemoveAttackDrone_currentIndexChanged(const QString &arg1)
{
    QString strSelectedDrone=ui->m_cmbRemoveAttackDrone->currentText();

   // qDebug() << "Selected Drone name " << strSelectedDrone;


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("AttackList");



    QStringList attackNames;
    QStringList  lstAttack =  setting.childGroups();

    setting.endGroup();


  //  qDebug() << "attack list " << lstAttack;

    for (auto attackname:lstAttack)
    {
        setting.beginGroup("AttackList");

        setting.beginGroup(attackname);

       // qDebug() << "attack name " << attackname;

        QString strDroneName=setting.value("DroneName","").toString();

       // qDebug() << "Drone name " << strDroneName;

        if(strDroneName != strSelectedDrone)
        {
            setting.endGroup();
            setting.endGroup();


            continue;
        }

        attackNames << attackname;

        setting.endGroup();
        setting.endGroup();

    }

   // qDebug() << "Drone Specific" << attackNames;

     ui->m_cmbxAttackDrone->clear();

    m_lstDronesListModel = new QStringListModel;
    m_lstDronesListModel->setStringList(attackNames);
    ui->m_cmbxAttackDrone->clear();
    ui->m_cmbxAttackDrone->setModel(m_lstDronesListModel);

    CLogger::getInstance()->log("Removal of selected Drone and it's respective attack operation completed.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbManageTools_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageTools);
    CLogger::getInstance()->log("Switched to Manage Tools UI.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbAddTools_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeAddTools);
    ui->m_leToolName->clear();
    ui->m_leToolPath->clear();
    ui->m_leToolName->setFocus();
    CLogger::getInstance()->log("Switched to Add Tools UI.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbRemoveTools_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeRemoveTools);

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("ToolList");

    m_lstAllTools =  setting.childGroups();
    m_lstToolsListModel = new QStringListModel;
    m_lstToolsListModel->setStringList(m_lstAllTools);
    ui->m_cmbxRemoveToolNames->setModel(m_lstToolsListModel);

    setting.endGroup();

    CLogger::getInstance()->log("Switched to Remove Tools UI.",QtDebugMsg);



}

void CDeveloperMode::on_m_pblaunchTools_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeLaunchTools);

    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("ToolList");

    m_lstAllTools =  setting.childGroups();
    m_lstToolsListModel = new QStringListModel;
    m_lstToolsListModel->setStringList(m_lstAllTools);
    ui->m_cmbxLaunchToolNames->setModel(m_lstToolsListModel);

    setting.endGroup();

    CLogger::getInstance()->log("Switched to Launch Tools UI.",QtDebugMsg);



}

void CDeveloperMode::on_m_pbAddNewTool_clicked()
{
    QString strName=ui->m_leToolName->text();
    QString strToolPath=ui->m_leToolPath->text();

    if( strName.isEmpty() || strToolPath.isEmpty())
    {
        QMessageBox::warning(this, "Alert","Please enter tool name / path");
        return;
    }


    bool bToolNameAlreadyExists=false;
    QSettings settingCheck(g_appSettingsPath,QSettings::NativeFormat);
    settingCheck.beginGroup("ToolList");
    QStringList lstAllTools =  settingCheck.childGroups();

    if(lstAllTools.contains(strName))
    {
        bToolNameAlreadyExists =true;
    }

    settingCheck.endGroup();


    if(bToolNameAlreadyExists)
    {
        QMessageBox::warning(this, "Alert", "Tool name already exists");
        return;
    }


    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("ToolList");
    setting.beginGroup(strName);
    setting.setValue("ToolName",strName);
    setting.setValue("ToolPath",strToolPath);
    setting.endGroup();
    setting.endGroup();

    setting.sync();
    QMessageBox::warning(this, "Alert", "Successfully added.");

    CLogger::getInstance()->log("New Tool added successfully.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbAddToolBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageTools);

    CLogger::getInstance()->log("Switched to manage tools UI.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbRemoveTool_clicked()
{

        if(ui->m_chkRemoveAllTools->isChecked())
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, "Tool", "Do you want to remove all Tools?",
                                          QMessageBox::Yes|QMessageBox::No);
            if (reply != QMessageBox::Yes)
            {
                return;
            }
        }

        QString strSelectedTool= ui->m_cmbxRemoveToolNames->currentText();
        QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
        setting.beginGroup("ToolList");
        if(ui->m_chkRemoveAllTools->isChecked())
        {
            setting.remove("");
        }else
        {

           setting.remove(strSelectedTool);

        }
        setting.endGroup();

        QMessageBox::information(this,"Alert","Tool data removd successfully.");

        setting.beginGroup("ToolList");


        setting.endGroup();


        setting.beginGroup("ToolList");

        m_lstAllTools =  setting.childGroups();
        m_lstToolsListModel = new QStringListModel;
        m_lstToolsListModel->setStringList(m_lstAllTools);
        ui->m_cmbxRemoveToolNames->clear();
        ui->m_cmbxRemoveToolNames->setModel(m_lstToolsListModel);
        setting.endGroup();

        CLogger::getInstance()->log("Removal of Tool completed.",QtDebugMsg);



}

void CDeveloperMode::on_m_pbRemoveToolBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageTools);
    CLogger::getInstance()->log("Switched to manage tools UI.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbLaunchTool_clicked()
{

    QString strSelectedTool= ui->m_cmbxLaunchToolNames->currentText();
    QSettings setting(g_appSettingsPath,QSettings::NativeFormat);
    setting.beginGroup("ToolList");
    setting.beginGroup(strSelectedTool);

    qDebug() << "strSelectedTool"<< strSelectedTool;

    QString strToolName= setting.value("ToolName","").toString();
    QString strToolPath = setting.value("ToolPath","").toString();
    setting.endGroup();
    setting.endGroup();

    if(strToolName.isEmpty() || strToolPath.isEmpty())
    {
        QMessageBox::warning(this, "Alert","unable to launch selected tool."+strToolName+"\n"+strToolPath);
        return;
    }

    if("dh-lin-x64-v1.11" == strSelectedTool)
    {

        QMessageBox::information(this, "Tool", "Drone Hack tool will be launched. Please connect the Drone to Computer?",QMessageBox::Ok);

        QEventLoop loop;
        QTimer::singleShot(4*1000,&loop,SLOT(quit()));
        loop.exec();

    }


    QFile(strToolPath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ExeOwner);

    QProcess *process= new QProcess;
    QStringList lstArg;
    lstArg.clear();
    process->startDetached(strToolPath,lstArg);

    CLogger::getInstance()->log("Tool launched.",QtDebugMsg);



}

void CDeveloperMode::on_m_pbLaunchToolBack_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeManageTools);
    CLogger::getInstance()->log("Switched to manage tools UI.",QtDebugMsg);


}

void CDeveloperMode::on_m_pbBackManageTools_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeOptions);
    CLogger::getInstance()->log("Switched to admin mode options UI.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbNextManageTools_clicked()
{
    ui->m_swDevMode->setCurrentWidget(ui->DevModeAddTools);
    CLogger::getInstance()->log("Switched to Add tools UI.",QtDebugMsg);

}

void CDeveloperMode::on_m_pbBrowse_clicked()
{
    QString strFileName = QFileDialog::getOpenFileName(this,
        tr("Select Executable"), QDir::homePath()+QDir::separator()+"Documents", tr("Executable Files (*.*)"));

    if(strFileName.isEmpty())
    {
        QMessageBox::warning(this, "Alert","Please select tool path");
        return;
    }

   ui->m_leToolPath->setText(strFileName);

   CLogger::getInstance()->log("Selected tool using browse option,",QtDebugMsg);


}

void CDeveloperMode::on_m_chkShowAddRemoveTools_clicked(bool checked)
{
    ui->m_pbAddTools->setVisible(checked);
    ui->m_pbRemoveTools->setVisible(checked);
    ui->m_pbNextManageTools->setVisible(checked);
}

void CDeveloperMode::on_m_pbToolInfo_clicked()
{
    QDesktopServices::openUrl(QUrl(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Tools_Info.txt"));
}
