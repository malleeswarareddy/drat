#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QMessageLogger>
#include <QLoggingCategory>
#include <QFile>

//Q_DECLARE_LOGGING_CATEGORY(logDebug)
Q_DECLARE_LOGGING_CATEGORY(logInfo)
Q_DECLARE_LOGGING_CATEGORY(logWarning)
Q_DECLARE_LOGGING_CATEGORY(logCritical)
Q_DECLARE_LOGGING_CATEGORY(logFatal)

class CLogger : public QObject
{


    Q_OBJECT
public:
    /**
     * @brief CLogger : explicit constructor class
     * @param parent
     */
    explicit CLogger(QObject *parent = nullptr);

    /**
      *@brief  ~CLogger() : desturctor of the class
      */
    ~CLogger();

    /**
     * @brief getInstance : get single instance of the object, it can used used in anywhere in the application
     * @return
     */
    static CLogger* getInstance();

    /**
     * @brief deleteInstance :  delete signle instance of the object
     */
    static void deleteInstance();

    /**
     * @brief log :  write log data to file
     * @param strData : log data
     * @param type :  log type
     */
    void log(QString strData, QtMsgType type=QtInfoMsg);

signals:

public slots:

private:

    /**
     * @brief m_logger : signle obbject instance.
     */
    static CLogger *m_logger;

    /**
     * @brief m_strLogFileName : to set log file path location to save logs to file.
     */
    QString m_strLogFileName;

};

#endif // LOGGER_H
