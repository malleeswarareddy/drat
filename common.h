#ifndef COMMON_H
#define COMMON_H

/**
 ** This header contains application required files path
 **/
#include <QDir>

const QString g_appSettingsPath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/dratsettings.ini";
const QString g_csvReportPath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/Reports/report.csv";
const QString g_hackTooPath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/Tools/dh-lin-x64-v1.11";
const QString g_BluetoothInstructionFilePath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/Bluetooth_Instructions.txt";
const QString g_GPSInstructionFilePath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/GPS_attack_instructions.txt";
const QString g_ReadmeHtmlFilePath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/Readme.html";
const QString g_WLANscanListFilePath=QDir::homePath()+QDir::separator()+"Documents/Dronetest/wlanscanlist.txt";
#endif // COMMON_H
