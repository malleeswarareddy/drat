#include "drat.h"
#include "ui_drat.h"

#include <QGuiApplication>
#include <QDesktopWidget>
#include <QScreen>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QDesktopServices>
#include <QProcess>

#include "logger.h"
#include "common.h"

CDRAT::CDRAT(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::DRAT)
{
    ui->setupUi(this);
    ui->m_swPages->setCurrentIndex(0);
    ui->m_pbReadmeFile->setToolTip(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Readme.txt file will open");
}

CDRAT::~CDRAT()
{
    CLogger::deleteInstance();
    delete ui;
}


void CDRAT::on_m_cmbxSelectmode_currentIndexChanged(int index)
{
    //0 - mode select
    //1- user mode
    //2 - developer mode
    switch (index) {

    case 0:
        break;

    case 1:
    {
        if(nullptr ==theUserMode)
        {
            theUserMode= new CUserMode(this);
            connect(theUserMode,SIGNAL(sigUserModeBack()),this,SLOT(slotGoToWelcomePage()));
        }

        ui->m_vlUserMode->insertWidget(0,theUserMode);
        ui->m_swPages->setCurrentIndex(1);

    }
        break;

    case 2:
    {

        if( nullptr == theDevMode )
        {
            theDevMode= new CDeveloperMode(this);
            connect(theDevMode,SIGNAL(sigDevModeBack()),this,SLOT(slotGoToWelcomePage()));
        }

        ui->m_vlDevMode->insertWidget(0,theDevMode);
        ui->m_swPages->setCurrentIndex(2);

    }
        break;
    case 3:
    {

        QString strScriptPath= QDir::homePath()+QDir::separator()+"Documents/Dronetest/Scripts/tshark.py";
        QProcess *process= new QProcess;
        QString strpython="gnome-terminal";
        QStringList lstArg;
        lstArg << "--" << "bash"  << "-c" << " "+strScriptPath+"; exec bash" ;

        qDebug() << strpython << strScriptPath;
        process->startDetached(strpython,lstArg);

        CLogger::getInstance()->log("Tool launched.",QtDebugMsg);

    }
        break;

    default:
        break;

    }
}

void CDRAT::slotGoToWelcomePage()
{
    ui->m_cmbxSelectmode->setCurrentIndex(0);
    ui->m_swPages->setCurrentIndex(0);
}

void CDRAT::on_m_pbReadmeFile_clicked()
{
    if(QFile::exists(g_ReadmeHtmlFilePath))
    {
        QFile file(g_ReadmeHtmlFilePath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
          QMessageBox::warning(this, "Alert", "No read me file present");
          return;
        }
        ui->textBrowser->setHtml(file.readAll());
        file.close();


        ui->m_swPages->setCurrentWidget(ui->m_pageAboutReadme);
    }else   if(QFile::exists(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Readme.txt"))
    {
        QDesktopServices::openUrl(QUrl(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Readme.txt"));
        CLogger::getInstance()->log("Launched readme.txt file",QtDebugMsg);
    }else
    {
        QMessageBox::warning(this, "Alert", "No read me file present");
    }

}

void CDRAT::on_m_pbAboutPageBack_clicked()
{
    ui->m_swPages->setCurrentWidget(ui->page);
}
