#include "drat.h"

#include <QApplication>
#include <QDebug>
#include <QGuiApplication>
#include <QScreen>
#include <QStyle>
#include <QDesktopWidget>
#include <QDir>
#include <QFile>
#include "logger.h"
#include "common.h"


/********************************************Logger Usage***************************
 *  CLogger::getInstance()->log("***********Mainwindow started**********1",QtDebugMsg);
    CLogger::getInstance()->log("***********Mainwindow started**********2",QtFatalMsg);
    CLogger::getInstance()->log("***********Mainwindow started**********3",QtInfoMsg);
    CLogger::getInstance()->log("***********Mainwindow started**********4",QtWarningMsg);
    CLogger::getInstance()->log("***********Mainwindow started**********5",QtCriticalMsg);
    **************************************END*********************************************/
int main(int argc, char *argv[])
{
    CLogger::getInstance()->log("***********started**********",QtDebugMsg);
    QApplication app(argc, argv);

    app.setApplicationVersion("1.6");
    CLogger::getInstance()->log("Application Version: 1.6",QtDebugMsg);
    CLogger::getInstance()->log("Application Display Version: 1.6",QtDebugMsg);

    app.setOrganizationDomain("");//TODO
    app.setApplicationName("Drone Attack Tool");
    CLogger::getInstance()->log("Application Name: Drone Attack Tool",QtDebugMsg);

    app.setApplicationDisplayName("ver 1.6");

    CLogger::getInstance()->log("Creating Documents,Documents/Scripts,Documents/Images and Documents/Log directories",QtDebugMsg);

    QDir dir;
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/");
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/");
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Scripts");
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Images");
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Log");
    dir.mkpath(QDir::homePath()+QDir::separator()+"Documents/Dronetest/Tools");

    if(!QFile::exists(g_hackTooPath))
    {
       QFile::copy("://Resources/dh-lin-x64-v1.11" , g_hackTooPath);
    }

    if(!QFile::exists(g_BluetoothInstructionFilePath))
    {
       QFile::copy("://Resources/Bluetooth_Instructions.txt" , g_BluetoothInstructionFilePath);
    }

    if(!QFile::exists(g_GPSInstructionFilePath))
    {
       QFile::copy("://Resources/GPS_attack_instructions.txt" , g_GPSInstructionFilePath);
    }

    if(!QFile::exists(g_ReadmeHtmlFilePath))
    {
       QFile::copy("://Resources/Readme.html" , g_ReadmeHtmlFilePath);
    }

    if(!QFile::exists(g_WLANscanListFilePath))
    {
       QFile::copy(":/Resources/wlanscanlist.txt" , g_WLANscanListFilePath);
    }


    //set permission
    if(QFile::exists(g_hackTooPath))
    {
        QFile(g_hackTooPath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner| QFileDevice::ExeOwner);;
    }

    if(QFile::exists(g_BluetoothInstructionFilePath))
    {
        QFile(g_BluetoothInstructionFilePath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);;
    }

    if(QFile::exists(g_GPSInstructionFilePath))
    {
        QFile(g_GPSInstructionFilePath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);;
    }

    if(QFile::exists(g_ReadmeHtmlFilePath))
    {
        QFile(g_ReadmeHtmlFilePath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);;
    }

    if(QFile::exists(g_WLANscanListFilePath))
    {
       QFile(g_WLANscanListFilePath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);
    }

    //update file permission




    CLogger::getInstance()->log("Directory creation ended here",QtDebugMsg);

    CLogger::getInstance()->log("Launching DRAT UI",QtDebugMsg);

    CDRAT w;
    w.move((QGuiApplication::primaryScreen()->availableGeometry().width()-800)/2,(QGuiApplication::primaryScreen()->availableGeometry().height()-800)/2);
    w.show();
    CLogger::getInstance()->log("Launching DRAT UI ended here",QtDebugMsg);


    CLogger::getInstance()->log("***********Ended**********",QtDebugMsg);
    return app.exec();
}
